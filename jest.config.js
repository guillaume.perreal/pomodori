module.exports = {
  preset: "ts-jest",
  testEnvironment: "node",
  globals: {
    "ts-jest": {
      tsConfig: "tsconfig.json",
    },
  },
  collectCoverageFrom: [
    "src/**/*.{ts,tsx}",
    "!**/node_modules/**",
    "!**/vendor/**",
  ],
  coverageDirectory: "reports/",
  coverageReporters: ["text-summary", "lcov", "istanbul-reports/lib/cobertura"],
  reporters: ["default", ["jest-junit", { outputDirectory: "reports/" }]],
  testResultsProcessor: "jest-sonar-reporter",
};
