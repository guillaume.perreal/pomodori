# Pomodori

Application web pour gérer son temps selon la méthode des pomodori,
avec intégration Gitlab.

# Features

- Gestion des pomodori :
  - [ ] Cycle travail-pause.
  - [ ] Affichage du temps restant.
  - [ ] Pause plus longue tous les X pomodoris.
  - [ ] Durées (pomodori, pauses, pauses longues) ajustables.
  - [ ] Notification sonore et par pop-up.
- Intégration avec Gitlab :
  - [ ] Authentification via OAuth.
  - [ ] Possibilité de choisir une tâche dans la liste des todos (ticket ?).
  - [ ] Reporte le temps passé dans la tâche sélectionnée dans Gitlab.

# Décisions d'architecture

- Organisation des sources :
  - Le code source se trouve dans src/.
  - Les assets (images, ...) se trouvent dans src/assets/.
  - Le code en rapport avec le domaine métier se trouve dans src/models/.
  - Les composants Web se trouvent dans src/components/.
  - Les tests se trouvent à côté des fichiers qu'ils testent ; ils ont pour extension .spec.ts.
  - Le résultat de compilation est stocké dans public/.
  - La configuration des outils se trouve dans des fichiers dédiés (pas dans le package.json), de préférences
    en JSON, ou en javascript si nécessaire ; éviter le YAML.
  - Dans la mesure du possible (= si ça ne fait pas planter l'outil), les fichiers .JSON devront commencer
    par clef `$schema` pointant sur un schéma (cf. https://www.schemastore.org).
- Language et outils :
  - Typescript 3.9 est le langue principal.
  - Les stylesheet sont écrite en SASS (.scss).
  - Webpack est utilisé pour "compiler" l'application.
  - Prettier et eslint sont utilisés pour formatter et vérifier le code.
  - Jest est le test-runner.
  - Husky et lint-staged sont utilisés pour vérifier les sources au moment du commit.
  - Les vérifications, les tests et la compilation sont exécutés en intégration continue pour chaque commit.

# Licence

Pomodori est couvert par la [licence AGPL v3.0](LICENSE.md)

# Crédits

Logo par [Icongeek26](https://www.flaticon.com/authors/icongeek26) de [Flaticon](https://www.flaticon.com/).
