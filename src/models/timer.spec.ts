import { actions, reducer, Timer } from "./timer";

describe("timer actions", () => {
  const NOW = 1000;
  const LATER = 1001;

  function create(
    status: Timer["status"],
    duration: Timer["duration"] = 1,
    elapsed: Timer["elapsed"] = 0,
    lastUpdate: Timer["lastUpdate"] = NOW
  ): Timer {
    return { status, duration, elapsed, lastUpdate };
  }

  describe("reset", () => {
    it("should create a stopped timer", () => {
      const timer = reducer(create("stopped"), actions.reset(10));

      expect(timer.duration).toEqual(10);
      expect(timer.elapsed).toEqual(0);
      expect(timer.status).toEqual("paused");
    });
  });

  describe("start", () => {
    it("should start a paused timer", () => {
      const timer = reducer(create("paused"), actions.start(LATER));

      expect(timer.status).toEqual("running");
      expect(timer.elapsed).toEqual(0);
    });

    it("should update a running timer", () => {
      const timer = reducer(create("running"), actions.start(LATER));

      expect(timer.status).toEqual("running");
      expect(timer.elapsed).toEqual(1);
    });

    it("should not start a stopped timer", () => {
      const timer = reducer(create("stopped"), actions.stop(LATER));

      expect(timer.status).toEqual("stopped");
      expect(timer.elapsed).toEqual(0);
    });
  });

  describe("pause", () => {
    it("should update and pause a running timer", () => {
      const timer = reducer(create("running"), actions.pause(LATER));

      expect(timer.status).toEqual("paused");
      expect(timer.elapsed).toEqual(1);
    });

    it("should not change a paused timer", () => {
      const timer = reducer(create("paused"), actions.pause(LATER));

      expect(timer.status).toEqual("paused");
      expect(timer.elapsed).toEqual(0);
    });

    it("should not start a stopped timer", () => {
      const timer = reducer(create("stopped"), actions.pause(LATER));

      expect(timer.status).toEqual("stopped");
      expect(timer.elapsed).toEqual(0);
    });
  });

  describe("stop", () => {
    it("should update and stop a running timer", () => {
      const timer = reducer(create("running"), actions.stop(LATER));

      expect(timer.status).toEqual("stopped");
      expect(timer.elapsed).toEqual(1);
    });

    it("should stop a paused timer", () => {
      const timer = reducer(create("paused"), actions.stop(LATER));

      expect(timer.status).toEqual("stopped");
      expect(timer.elapsed).toEqual(0);
    });

    it("should not change a stopped timer", () => {
      const timer = reducer(create("stopped"), actions.stop(LATER));

      expect(timer.status).toEqual("stopped");
      expect(timer.elapsed).toEqual(0);
    });
  });

  describe("tick", () => {
    it("should update a running timer", () => {
      const timer = reducer(create("running"), actions.tick(LATER));

      expect(timer.status).toEqual("running");
      expect(timer.elapsed).toEqual(1);
    });

    it("should not change a paused timer", () => {
      const timer = reducer(create("paused"), actions.tick(LATER));

      expect(timer.status).toEqual("paused");
      expect(timer.elapsed).toEqual(0);
    });

    it("should not change a stopped timer", () => {
      const timer = reducer(create("stopped"), actions.tick(LATER));

      expect(timer.status).toEqual("stopped");
      expect(timer.elapsed).toEqual(0);
    });
  });
});
