import { Draft, produce } from "immer";

export const now = (): number => Date.now();

export type Timer = Readonly<{
  status: "paused" | "stopped" | "running";
  duration: number;
  elapsed: number;
  lastUpdate: number;
}>;

export type Action = Readonly<
  | { type: "start" | "stop" | "pause" | "tick"; when: number }
  | { type: "reset"; duration: number }
>;

export const actions = {
  start: (when = now()): Action => ({ type: "start", when }),
  stop: (when = now()): Action => ({ type: "stop", when }),
  pause: (when = now()): Action => ({ type: "pause", when }),
  tick: (when = now()): Action => ({ type: "tick", when }),
  reset: (duration: number): Action => ({ type: "reset", duration }),
};

export const reducer: (t: Timer | undefined, a: Action) => Timer = produce(
  (timer: Draft<Timer>, action: Action): Timer | void => {
    if (action.type === "reset") {
      return {
        status: "paused",
        duration: action.duration,
        elapsed: 0.0,
        lastUpdate: now(),
      };
    }
    if (timer.status === "running") {
      timer.elapsed = action.when - timer.lastUpdate;
      timer.lastUpdate = action.when;
    }
    if (action.type === "start" && timer.status === "paused") {
      timer.status = "running";
    } else if (action.type === "pause" && timer.status === "running") {
      timer.status = "paused";
    } else if (action.type === "stop" && timer.status !== "stopped") {
      timer.status = "stopped";
    }
  },
  {
    status: "stopped",
    duration: 1,
    elapsed: 1,
    lastUpdate: now(),
  }
);
